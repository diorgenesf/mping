<?php 
	$servername = "127.0.0.1";
	$username = "monitoramento";
	$password = "5x9fxfvRbT"; 
	$dbname  = "mping";
	$conn = null;

	try 
	{
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		// set the PDO error mode to exception
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e)
	{
		echo 'Error: ' . $e->getMessage();
	}

	$sqlConfig = "SELECT * FROM `configuracoes`";
	try{
			$resultConfig = $conn->query($sqlConfig);
		}
	catch(PDOException $e)
	{
		echo 'Error: ' . $e->getMessage();
	}
	$configuracoes = $resultConfig->fetchAll(PDO::FETCH_ASSOC);

	$queryBase = "ping ";
	foreach ($configuracoes as $config) {
		if($config['modulo']=="ping"){
			$queryBase .= $config['trigger']." ".$config['valor']." ";
		}
	}

	$sql = "SELECT * FROM `servidores`";
	try{
		$result = $conn->query($sql);
	}
	catch(PDOException $e)
	{
		echo 'Error: ' . $e->getMessage();
	}
	$servers = $result->fetchAll(PDO::FETCH_ASSOC);
	foreach ($servers as $server) {
		$queryPing = $queryBase.$server['ip'];
	    $saida = shell_exec($queryPing);
	    $saidas = explode("\n", $saida);
		$pings = array();
		$media = 0;

		for($i=1;$i<11;$i++)
		{
			$ping = substr(explode(" ", $saidas[$i] )[6],5);
			$pings[] = $ping;
			$media+= $ping;
		}
		$media/=10;
	    try{
	    	$stmt = $conn->prepare('INSERT INTO pings (servidor,data,media)  
	    							VALUES(?,NOW(),?)'); 

	    	$stmt->bindParam(1, $server['id']);
	    	$stmt->bindParam(2, $media);
	    	$stmt->execute();
	    }catch(PDOException $e)
	    {
			echo 'Error: ' . $e->getMessage();
	    }
	}
	$conn = null;
?>