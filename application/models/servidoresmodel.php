<?php 

class Servidoresmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

	public function insert($servidor = NULL)
	{
		if($servidor == NULL) return false;
		$this->db->insert('servidores', $servidor); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function deleteById($id = NULL)
	{
		if($id == NULL) return false;
		$this->db->delete('servidores', array('id' => $id)); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function selectAll()
	{
		$query = $this->db->get('servidores');
		return $query->result();
	}

	public function selectById($id = NULL,$limit)
	{
		if($id==NULL) return false;
		$this->db->order_by("id", "desc");
		$this->db->limit($limit);
		$query = $this->db->get_where('servidores', array('id' => $id));
		return $query->result();
	}

	/*
	public function selectLikeNome($nome = NULL)
	{
		if($nome==NULL) return false;
		$this->db->select('*');
		$this->db->from('servidores');
		$this->db->like('nome', $nome);
		return $this->db->get()->result_array();
	}
	*/

	public function updateById($servidor = NULL,$id = NULL)
	{
		if($servidor==NULL || $id == NULL) return false;
		$this->db->update('servidores', $servidor, array('id' => $id));
		if($this->db->affected_rows()==0) return false;
		else return true;
	}

}