<?php 

class Pingsmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

	public function insert($ping = NULL)
	{
		if($ping == NULL) return false;
		$this->db->insert('pings', $ping); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function deleteById($id = NULL)
	{
		if($id == NULL) return false;
		$this->db->delete('pings', array('id' => $id)); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function selectAll()
	{
		$query = $this->db->get('pings');
		return $query->result();
	}

	public function selectById($servidor = NULL,$horaInicio = NULL,$horaFim = NULL)
	{
		if($servidor==NULL || $horaInicio == NULL || $horaFim == NULL) return false;
		$sql = "SELECT * FROM pings 
				WHERE servidor = '".$servidor."' 
				AND data >= '".$horaInicio."' 
				AND data <= '".$horaFim."' ORDER BY data ASC";
		
		$query = $this->db->query($sql);
		return $query->result();
	}

	/*
	public function selectLikeNome($nome = NULL)
	{
		if($nome==NULL) return false;
		$this->db->select('*');
		$this->db->from('pings');
		$this->db->like('nome', $nome);
		return $this->db->get()->result_array();
	}
	*/

	public function updateById($servidor = NULL,$id = NULL)
	{
		if($servidor==NULL || $id == NULL) return false;
		$this->db->update('pings', $servidor, array('id' => $id));
		if($this->db->affected_rows()==0) return false;
		else return true;
	}

}