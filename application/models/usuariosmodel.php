<?php 

class Usuariosmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

	public function insert($user = NULL)
	{
		if($user == NULL) return false;
		$this->db->insert('usuarios', $user); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function deleteByNick($nick = NULL)
	{
		if($nick == NULL) return false;
		$this->db->delete('usuarios', array('nick' => $nick)); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function selectAll()
	{
		$query = $this->db->get('usuarios');
		return $query->result();
	}

	public function selectByNick($nick = NULL)
	{
		if($nick==NULL) return false;
		$query = $this->db->get_where('usuarios', array('nick' => $nick));
		return $query->result();
	}

	public function selectLikeNome($nome = NULL)
	{
		if($nome==NULL) return false;
		$this->db->select('*');
		$this->db->from('usuarios');
		$this->db->like('nome', $nome);
		return $this->db->get()->result_array();
	}

	public function updateByNick($user = NULL,$nick = NULL)
	{
		if($user==NULL || $nick == NULL) return false;
		$this->db->update('usuarios', $user, array('nick' => $nick));
		if($this->db->affected_rows()==0) return false;
		else return true;
	}

}