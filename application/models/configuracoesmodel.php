<?php 

class Configuracoesmodel extends CI_Model {
    function __construct()
    {
        parent::__construct();
    }

	public function insert($config = NULL)
	{
		if($config == NULL) return false;
		$this->db->insert('configuracoes', $config); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function deleteById($id = NULL)
	{
		if($id == NULL) return false;
		$this->db->delete('configuracoes', array('id' => $id)); 
		if($this->db->affected_rows()==0) return false;
		else return true;
	}
	
	public function selectAll()
	{
		$query = $this->db->get('configuracoes');
		return $query->result();
	}

	public function selectById($id = NULL)
	{
		if($id==NULL) return false;
		$query = $this->db->get_where('configuracoes', array('id' => $id));
		return $query->result();
	}

	public function selectLikeVariavel($variavel = NULL)
	{
		if($variavel==NULL) return false;
		$this->db->select('*');
		$this->db->from('configuracoes');
		$this->db->like('variavel', $variavel);
		return $this->db->get()->result_array();
	}

	public function updateById($config = NULL,$id = NULL)
	{
		if($config==NULL || $id == NULL) return false;
		$this->db->update('configuracoes', $config, array('id' => $id));
		if($this->db->affected_rows()==0) return false;
		else return true;
	}

}