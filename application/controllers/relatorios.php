<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Relatorios extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Servidoresmodel');
		$this->load->model('Pingsmodel');
		$this->load->model('Configuracoesmodel');
	}

	public function index()
	{	
		if(!$this->session->userdata('logged'))
		{
			header("location: ".base_url());
		}
		else 
		{
			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "relatorios";
			$meta['header_name'] = "Relatórios";
			$this->load->view('template',$meta);
		}
	}

	public function getDados()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
        	$pings = $this->Pingsmodel->selectById(
	        		$_GET['IdServidor'], 
	        		date('Y-m-d H:i:s',$_GET['dataInicio']), 
	        		date('Y-m-d H:i:s',$_GET['dataFim'])
        		);
			$DataFim['dias'] = date('d',$_GET['dataFim']);
			$DataFim['horas'] = date('H',$_GET['dataFim']);
			$DataFim['minutos'] = date('i',$_GET['dataFim']);
 			$data_points = array();

			foreach ($pings as $ping) {

				$DataInicio['dias'] = date('d',strtotime($ping->data));
				$DataInicio['horas'] = date('H',strtotime($ping->data));
				$DataInicio['minutos'] = date('i',strtotime($ping->data));

				$total = 0;
				$dias = ($DataFim['dias'] - $DataInicio['dias']);
				$dias *= 60; //Parse Hours
				$dias *= 60; //Parse Minutes

				$total += $dias;

				$horas = ($DataFim['horas'] - $DataInicio['horas']);
				$horas *= 60; //Parse Minutes
				$total += $horas;
				$total += ($DataFim['minutos'] - $DataInicio['minutos']);	

				$point = array("x" => -$total , "y" => $ping->media);
        
        		array_push($data_points, $point); 
			}
			
			echo json_encode($data_points, JSON_NUMERIC_CHECK);
        }
	}


	public function pesquisar()
	{
		if(!$this->session->userdata('logged') 
			|| !isset($_GET['DiaInicial'])
			|| !isset($_GET['HoraInicial'])
			|| !isset($_GET['DiaFinal'])
			|| !isset($_GET['HoraFinal'])
			|| !isset($_GET['TipoMedia'])
			)
		{
			header("location: ".base_url()."index.php/relatorios");
		}
		else 
		{
			$configuracoes = $this->Configuracoesmodel->selectAll();
			foreach ($configuracoes as $config) {
				if($config->modulo=="canvasjs")
				{
					$meta['TipoGrafico'] = $config->valor;
				}
			}

			$DataInicio = strtotime($_GET['DiaInicial']." ".$_GET['HoraInicial'].":00");
			$DataFim = strtotime($_GET['DiaFinal']." ".$_GET['HoraFinal'].":00");

			/* -------------------   Configurações de Pesquisa -----------------*/

			$meta['DiaInicial'] = $_GET['DiaInicial'];
			$meta['HoraInicial'] = $_GET['HoraInicial'];
			$meta['DiaFinal'] = $_GET['DiaFinal'];
			$meta['HoraFinal'] = $_GET['HoraFinal'];
			$meta['TipoMedia'] = $_GET['TipoMedia'];

			/* -------------------   Fim Configurações de Pesquisa -----------------*/

			$meta['DataInicio'] = $DataInicio;
			$meta['DataFim'] = $DataFim;
			
			$meta['servidores'] = $this->Servidoresmodel->selectAll();			
			$meta['TipoPesquisa'] = $_GET['TipoMedia'];
			$meta['PesquisaON'] = true;

			/* -------------------   Configurações da Página -----------------*/

			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "relatorios";
			$meta['header_name'] = "Relatórios";

			$this->load->view('template',$meta);

			/* -------------------   Fim Configurações da Página -----------------*/
			
		}
	}
	
}
