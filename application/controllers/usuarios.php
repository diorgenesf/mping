<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Usuariosmodel');
	}

	public function index()
	{	
		if(!$this->session->userdata('logged'))
		{
			header("location: ".base_url());
		}
		else 
		{
			$meta['usuarios'] = $this->Usuariosmodel->selectAll();
			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "usuarios";
			$meta['header_name'] = "Usuários";
			$this->load->view('template',$meta);
		}
	}

	public function add(){
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			$data['nick'] = $_POST['NickUsuario'];
			$data['nome'] = $_POST['NomeUsuario'];
			$data['senha'] = md5($_POST['SenhaUsuario']);
			
			if($this->Usuariosmodel->insert($data))
			{
				header("location: ".base_url()."index.php/usuarios");
			}
			else
			{
				header("location: ".base_url()."index.php/usuarios/?error_on_insert");
			}
		}
	}


	public function editar()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			if(!isset($_POST['NickUsuario']))
			{
				header("location: ".base_url()."index.php/usuarios");
			}
			else
			{
				if($_POST['SenhaUsuario']=="")
				{
					$user = $this->Usuariosmodel->selectByNick($_POST['NickUsuario']);
					$data['senha'] = $user[0]->senha;
				}
				else
				{
					$data['senha'] = md5($_POST['SenhaUsuario']);
				}
				$data['nome'] = $_POST['NomeUsuario'];
				if($this->Usuariosmodel->updateByNick($data,$_POST['NickUsuario']))
				{
					header("location: ".base_url()."index.php/usuarios");
				}
				else
				{
					header("location: ".base_url()."index.php/usuarios/?error_on_update");
				}
			}
		}
	}

	public function excluir()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			if(!isset($_POST['NickUsuario']))
			{
				header("location: ".base_url()."index.php/usuarios");
			}
			else
			{
				if($this->Usuariosmodel->deleteByNick($_POST['NickUsuario']))
				{
					header("location: ".base_url()."index.php/usuarios");
				}
				else
				{
					header("location: ".base_url()."index.php/usuarios/?error_on_delete");
				}
			}
		}
	}

	
}
