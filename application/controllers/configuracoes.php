<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Configuracoes extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Configuracoesmodel');
	}

	public function index()
	{	
		if(!$this->session->userdata('logged'))
		{
			header("location: ".base_url());
		}
		else 
		{
			$meta['configuracoes'] = $this->Configuracoesmodel->selectAll();
			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "configuracoes";
			$meta['header_name'] = "Configurações";
			$this->load->view('template',$meta);
		}
	}

	public function add(){
		header("location: ".base_url());

	}


	public function editar()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			if(!isset($_POST['IdConfiguracao']))
			{
				header("location: ".base_url()."index.php/configuracoes");
			}
			else
			{
				$data['valor'] = $_POST['ValorConfiguracao'];
				if($this->Configuracoesmodel->updateById($data,$_POST['IdConfiguracao']))
				{
					header("location: ".base_url()."index.php/configuracoes");
				}
				else
				{
					header("location: ".base_url()."index.php/configuracoes/?error_on_update");
				}
			}
		}
	}

	public function excluir()
	{
		header("location: ".base_url());
	}

	
}
