<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Usuariosmodel');
	}

	public function index()
	{	
		$meta['msg_error'] = FALSE;
		if(!$this->session->userdata('logged'))
		{
			if(isset($_GET['error']))
			{
				$meta['msg_error'] = TRUE;
			}
			$this->load->view('login',$meta);
		}
		else 
		{
			header("location: ".base_url()."index.php/monitoramento");
			/*
			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "monitoramento";
			$meta['header_name'] = "Monitoramento";
			$this->load->view('template',$meta);
			*/
		}
	}

	public function validation()
	{
		if( ( $_POST['username'] == '' ) || ( $_POST['password'] == '' ) )
		{
			header("location: ".base_url()."?error=1");
		}
		else
		{
			$user_data = $this->Usuariosmodel->selectByNick($_POST['username']);
			
			if($user_data)
			{
				if( $user_data[0]->senha==md5($_POST['password']) )
				{
					$this->session->set_userdata(array("nick"=>$user_data[0]->nick,
												"nome"=>$user_data[0]->nome,
												"senha"=>$user_data[0]->senha,
												"logged"=>TRUE)
												);
					header("location: ".base_url());
				}
				else header("location: ".base_url()."?error=3");
			}
			else header("location: ".base_url()."?error=2");
		}
	}


}
