<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sobre extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{	
		if(!$this->session->userdata('logged'))
		{
			header("location: ".base_url());
		}
		else 
		{
			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "sobre";
			$meta['header_name'] = "Sobre";
			$this->load->view('template',$meta);
		}
	}
	
}
