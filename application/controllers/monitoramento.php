<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Monitoramento extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('Servidoresmodel');
		$this->load->model('Pingsmodel');
		$this->load->model('Configuracoesmodel');
	}

	public function index()
	{	
		if(!$this->session->userdata('logged'))
		{
			header("location: ".base_url());
		}
		else 
		{
			$configuracoes = $this->Configuracoesmodel->selectAll();
			foreach ($configuracoes as $config) {
				if($config->modulo=="canvasjs")
				{
					$meta['TipoGrafico'] = $config->valor;
				}
			}
			$meta['servidores'] = $this->Servidoresmodel->selectAll();
			$meta['title'] = "Sistema de Monitoramento de Ping";
			$meta['view_name'] = "monitoramento";
			$meta['header_name'] = "Monitoramento";
			$this->load->view('template',$meta);
		}
	}

	public function add(){
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			$data['nome'] = $_POST['NomeServidor'];
			$data['ip'] = $_POST['IpServidor'];
			
			if($this->Servidoresmodel->insert($data))
			{
				header("location: ".base_url()."index.php/monitoramento");
			}
			else
			{
				header("location: ".base_url()."index.php/monitoramento/?error_on_insert");
			}
		}
	}


	public function editar()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			if(!isset($_POST['id']))
			{
				header("location: ".base_url()."usuarios");
			}
			else
			{
				$data['nome'] = $_POST['NomeServidor'];
				$data['ip'] = $_POST['IpServidor'];
				if($this->Servidoresmodel->updateById($data,$_POST['id']))
				{
					header("location: ".base_url()."index.php/monitoramento");
				}
				else
				{
					header("location: ".base_url()."index.php/monitoramento/?error_on_update");
				}
			}
		}
	}

	public function excluir()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			if(!isset($_POST['id']))
			{
				header("location: ".base_url()."usuarios");
			}
			else
			{
				if($this->Servidoresmodel->deleteById($_POST['id']))
				{
					header("location: ".base_url()."index.php/monitoramento");
				}
				else
				{
					header("location: ".base_url()."index.php/monitoramento/?error_on_delete");
				}
			}
		}
	}

	public function getDados()
	{
		if(!$this->session->userdata('logged'))
        {
            header("location: ".base_url());
        }
        else 
        {
			$time = time();
			$diference = 120; //minutes
			$H = date('H',$time); 
			$H -=	($diference/60);
			$OldTime = mktime($H,date('i',$time),date('s',$time),date('m',$time),date('d',$time),date('Y',$time));

        	$pings = $this->Pingsmodel->selectById($_GET['IdServidor'], date('Y-m-d H:i:s',$OldTime), date('Y-m-d H:i:s',$time));
			$DataFim['horas'] = date('H',$time);
			$DataFim['minutos'] = date('i',$time);
 			$data_points = array();

			foreach ($pings as $ping) {

				$DataInicio['horas'] = date('H',strtotime($ping->data));
				$DataInicio['minutos'] = date('i',strtotime($ping->data));

				$total = 0;
				$total += ($DataFim['horas'] - $DataInicio['horas']);
				$total *= 60;
				$total += ($DataFim['minutos'] - $DataInicio['minutos']);	

				$point = array("x" => -$total , "y" => $ping->media);
        
        		array_push($data_points, $point); 

				//echo "{ x: -".$total.", y: ".$ping->media." },";
			}
			
			echo json_encode($data_points, JSON_NUMERIC_CHECK);
        }
	}
	
}
