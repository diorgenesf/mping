<script type="text/javascript" src="<?php echo base_url(); ?>public/js/canvasjs.min.js"></script>
<div id="monitoramento">
	<style type="text/css">
		a.canvasjs-chart-credit{
			visibility: hidden;
		}
		form button{
			float: right;
		}
		.modal-body .form-group{
			text-align: left;		
		}
		.modal-body .form-group label{
			padding-left: 5px;
		}
		.modal-footer button:last-child{
			margin-right: 10px;
		}
	</style>

	<div class="box">
		<div class="bheader">
			Adicionar Servidor
		</div>
		<div class="bcontent">
			<form method="post" action="<?php echo base_url(); ?>index.php/monitoramento/add" role="form">
				<div class="form-group">
					<label for="NomeServidor">Nome</label>
					<input type="text" class="form-control" name="NomeServidor" id="NomeServidor" placeholder="Informe o nome do Servidor">
				</div>
				<div class="form-group">
					<label for="IpServidor">IP</label>
					<input type="text" class="form-control" name="IpServidor" id="IpServidor" placeholder="Informe o IP do Servidor">
				</div>
				<button type="submit" class="btn btn-default">Salvar</button>
			</form>
		</div>
	</div>
	<?php 
		foreach ($servidores as $servidor) {
			
			?>
				<div class="box">
					<div class="bheader">
						<?php echo $servidor->nome; ?>
						<span class="ip">(<?php echo $servidor->ip; ?>)</span>
						<span class="icones">
							<!-- Editar -->
							<a data-toggle="modal" href="#edit-<?php echo $servidor->id; ?>" class="glyphicon glyphicon-edit icon"></a>
							<!-- Inicio Modal-->
							<div class="modal fade" id="edit-<?php echo $servidor->id; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $servidor->id; ?>Label" aria-hidden="true">
							  <div class="modal-dialog">
							  	<form role="form" action="<?php echo base_url(); ?>index.php/monitoramento/editar" method="post">
								    <div class="modal-content">
								      <div class="modal-header">
								        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
								        <h4 class="modal-title" id="<?php echo $servidor->id; ?>Label">Editar Servidor: <?php echo $servidor->nome; ?></h4>
								      </div>
								      <div class="modal-body">
								        	<input name="id" type="hidden" value="<?php echo $servidor->id; ?>">

											<div class="form-group">
												<label for="NomeServidor">Nome</label>
												<input type="text" class="form-control" name="NomeServidor" placeholder="Informe o nome do Servidor" value="<?php echo $servidor->nome; ?>">
											</div>
											<div class="form-group">
												<label for="IpServidor">IP</label>
												<input type="text" class="form-control" name="IpServidor" placeholder="Informe o IP do Servidor" value="<?php echo $servidor->ip; ?>">
											</div>
											
								      </div>
								      <div class="modal-footer">
								        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
								        <button type="submit" class="btn btn-primary">Salvar Alterações</button>
								      </div>
								    </div> <!-- FIM: div /.modal-content -->
							    </form>

							  </div> <!-- FIM: div /.modal-dialog -->
							</div>
							<!-- Fim Inicio Modal-->
							<!-- FIM Editar -->

							<!-- Excluir -->
							<a data-toggle="modal" href="#delete-<?php echo $servidor->id; ?>" class="glyphicon glyphicon-trash icon"></a>
							<!-- Inicio Modal-->
							<div class="modal fade" id="delete-<?php echo $servidor->id; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $servidor->id; ?>Label" aria-hidden="true">
								<div class="modal-dialog">
									<form role="form" action="<?php echo base_url(); ?>index.php/monitoramento/excluir" method="post">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
												<h4 class="modal-title" id="<?php echo $servidor->id; ?>Label">Excluir Servidor: <?php echo $servidor->nome; ?></h4>
											</div>
											<div class="modal-body">
												<input name="id" type="hidden" value="<?php echo $servidor->id; ?>">

												<div class="form-group">
												<p lingdex="0">Você tem certeza que deseja excluir esse servidor?</p>
												</div>

											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-default"> Excluir</button>
												<button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
											</div>
										</div> <!-- FIM: div /.modal-content -->
									</form>

								</div> <!-- FIM: div /.modal-dialog -->
							</div>
							<!-- Fim Inicio Modal-->

							<!-- FIM Editar -->
						</span>
					</div>
					<div class="bcontent">
						<script type="text/javascript">
					        $(document).ready(function () {
					        	$.getJSON("<?php echo base_url('index.php/monitoramento/getDados?IdServidor=').$servidor->id;?>", function (result) {
					        		//console.log("Data: "+result);
					                var chart<?php echo $servidor->id; ?> = new CanvasJS.Chart("chartContainer<?php echo $servidor->id; ?>", {
					                    
										toolTip:{             
											content: function(e){
												var content;
												var tempo = e.entries[0].dataPoint.x;
												tempo *= -1;
												content = "<label class='bold'>Latência (ms):</label> "+e.entries[0].dataPoint.y + "<hr class='canvas'> <label class='bold'>Tempo (Minutos Passados):</label> "+tempo;
												return content;
											},
										},
										data: [
					                        {
					                        	type: "<?php echo $TipoGrafico; ?>",
					                            dataPoints: result
					                        }
					                    ]
					                });

					                chart<?php echo $servidor->id; ?>.render();
					            });
					        	
					            var updateInterval = (1000*60); //1 Minuto
								// update chart after specified time. 
								setInterval(function(){
									$.getJSON("<?php echo base_url('index.php/monitoramento/getDados?IdServidor=').$servidor->id;?>", function (result) {

						                var chart<?php echo $servidor->id; ?> = new CanvasJS.Chart("chartContainer<?php echo $servidor->id; ?>", {
						                    toolTip:{             
												content: function(e){
													var content;
													var tempo = e.entries[0].dataPoint.x;
													tempo *= -1;
													content = "<label class='bold'>Latência (ms):</label> "+e.entries[0].dataPoint.y + "<hr class='canvas'> <label class='bold'>Tempo (Minutos Passados):</label> "+tempo;
													return content;
												},
											},
											data: [
							                        {
							                        	type: "<?php echo $TipoGrafico; ?>",
							                            dataPoints: result
							                        }
							                    ]
							                });

						                chart<?php echo $servidor->id; ?>.render();
						            });
								}, updateInterval);
					        });
					    </script>
						<div class="chartContainer" id="chartContainer<?php echo $servidor->id; ?>"></div>

					</div>
					<!--
					<div class="bfooter">
						<div>Última Atualização</div>
					</div>
					-->
					
				</div>
			<?php 
		}

	?>
</div>