<div id="sobre">
	<style type="text/css">
		.bcontent p{
			text-align: justify;
		}
	</style>

	<div class="box">
		<div class="bheader">
			O mPing
		</div>
		<div class="bcontent">
			<p>
				O <b>mPing</b> é um sistema desenvolvido por <a href="#contato">Diorgenes Ferreira</a> 
				que tem como intuito medir níveis de latência de servidores cadastrados através 
				da captura de pings.<br />
			</p>
			<p>
				Através da coleta de pings, é calculada a média e armazenada em um banco de dados. 
				A coleta de dados permite a geração de dados que são exibidos por gráficos em tempo 
				real no menu <a href="<?php echo base_url(); ?>">Monitoramento</a>.<br />
			</p>
			<p>
				Outro valioso recurso do <b>mPing</b> é que ele permite a recuperação de latência 
				através de relatórios. <br />
			</p>

			<p>
				A medição de latência pode facilitar diversas coisas, para facilitar a utilização 
				do sistema para problemas diversos as principais configurações para medir latência 
				são encontradas no menu 
				<a href="<?php echo base_url(); ?>index.php/configuracoes">Configurações</a>.
			</p>
		</div>
	</div>

	<div class="box">
		<div class="bheader">
			Versão
		</div>
		<div class="bcontent">
			<p>
				O <b>mPing</b> está em sua versão BETA, onde serão testados as suas funcionalidades em um ambiente real e detectado sua eficiência. 
			</p>
		</div>
	</div>

	<div class="box">
		<div class="bheader">
			Documentação
		</div>
		<div class="bcontent">
			<p>
				A documentação do <b>mPing</b> será construída apenas a construção da sua primeira versão estável. Por tanto, quaisquer dúvidas devem ser tratadas com o desenvolvedor <a href="#contato">Diorgenes Ferreira</a>. 
			</p>
		</div>
	</div>

	<div class="box" id="contato">
		<div class="bheader">
			Desenvolvedor
		</div>
		<div class="bcontent">
			<p>
				Esse sistema foi idealizado e desenvolvido exclusivamente por <a href="#contato">Diorgenes Ferreira</a>. <br />
				Para mais informações com o desenvolvedor entre em contato: <br />
				<br />
				<b>E-mail: </b> diorgenesferreira1@hotmail.com <br />
				<b>Telefone: </b> (38) 9151-3769 <br />

			</p>
		</div>
	</div>


</div>