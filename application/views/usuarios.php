<div id="usuarios">
	<style type="text/css">
		form button{
			float: right;
		}
		table th:last-child,table .lastTD{
			text-align: center;
		}
		.modal-body .form-group{
			text-align: left;		
		}
		.modal-body .form-group label{
			padding-left: 5px;
		}
		.modal-footer button:last-child{
			margin-right: 10px;
		}
	</style>
	<div class="box">
		<div class="bheader">
			Adicionar Usuário
		</div>
		<div class="bcontent">
			<form method="post" action="<?php echo base_url(); ?>index.php/usuarios/add" role="form">
				<div class="form-group">
					<label for="NickUsuario">Nick</label>
					<input type="text" class="form-control" name="NickUsuario" id="NickUsuario" placeholder="Informe o nick do novo Usuário">
				</div>
				<div class="form-group">
					<label for="NomeUsuario">Nome</label>
					<input type="text" class="form-control" name="NomeUsuario" id="NomeUsuario" placeholder="Informe o nome do novo Usuário">
				</div>
				<div class="form-group">
					<label for="SenhaUsuario">Senha</label>
					<input type="password" class="form-control" name="SenhaUsuario" id="SenhaUsuario" placeholder="Informe a senha do novo Usuário">
				</div>
				<button type="submit" class="btn btn-default">Salvar</button>
			</form>
		</div>
	</div>

	<div class="box">
		<div class="bheader">
			Gerenciamento de Usuários
		</div>
		<div class="bcontent">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Nick</th>
						<th>Nome</th>
						<th colspan="2">Ação</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						foreach ($usuarios as $usuario) {
							$anick = str_replace(".", "-", $usuario->nick);
							?>
								<tr>
									<td><?php echo $usuario->nick; ?></td>
									<td><?php echo $usuario->nome; ?></td>
									<td class="lastTD">
										<a data-toggle="modal" href="#edit-<?php echo $anick; ?>" class="glyphicon glyphicon-edit"></a>
										
										<!-- Modal Editar -->
										<div class="modal fade" id="edit-<?php echo $anick; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $usuario->nick; ?>Label" aria-hidden="true">
										  <div class="modal-dialog">
										  	<form role="form" action="<?php echo base_url(); ?>index.php/usuarios/editar" method="post">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
											        <h4 class="modal-title" id="<?php echo $usuario->nick; ?>Label">Editar Usuário: <?php echo $usuario->nome; ?></h4>
											      </div>
											      <div class="modal-body">
											        	<input name="NickUsuario" type="hidden" value="<?php echo $usuario->nick; ?>">

														<div class="form-group">
															<label for="InputNomeCompleto-<?php echo $usuario->nick; ?>">Nome Completo</label>
															<input type="text" name="NomeUsuario" class="form-control" id="InputNomeCompleto-<?php echo $usuario->nick; ?>" placeholder="Informe o nome completo do Usuário" value="<?php echo $usuario->nome; ?>">
														</div>

														<div class="form-group">
															<label for="InputSenha-<?php echo $usuario->nick; ?>">Senha</label>
															<input type="password" name="SenhaUsuario" class="form-control" id="InputSenha-<?php echo $usuario->nick; ?>" placeholder="Informe uma nova senha" value="">
														</div>
														
											      </div>
											      <div class="modal-footer">
											        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
											        <button type="submit" class="btn btn-primary">Salvar Alterações</button>
											      </div>
											    </div> <!-- FIM: div /.modal-content -->
										    </form>

										  </div> <!-- FIM: div /.modal-dialog -->
										</div> 
										<!-- FIM: Modal Editar -->
									</td>
									<td class="lastTD">
										<a data-toggle="modal" href="#delete-<?php echo $anick; ?>" class="glyphicon glyphicon-trash"></a>

										<!-- Modal Excluir -->
										<div class="modal fade" id="delete-<?php echo $anick; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $usuario->nick; ?>Label" aria-hidden="true">
										  <div class="modal-dialog">
										  	<form role="form" action="<?php echo base_url(); ?>index.php/usuarios/excluir" method="post">
											    <div class="modal-content">
											      <div class="modal-header">
											        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
											        <h4 class="modal-title" id="<?php echo $usuario->nick; ?>Label">Excluir Usuário: <?php echo $usuario->nome; ?></h4>
											      </div>
											      <div class="modal-body">
											        	<input name="NickUsuario" type="hidden" value="<?php echo $usuario->nick; ?>">

														<div class="form-group">
															<p lingdex="0">Você tem certeza que deseja excluir esse usuário?</p>
														</div>
														
											      </div>
											      <div class="modal-footer">
											        <button type="submit" class="btn btn-default"> Excluir</button>
											        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
											      </div>
											    </div> <!-- FIM: div /.modal-content -->
										    </form>

										  </div> <!-- FIM: div /.modal-dialog -->
										</div> 
										<!-- FIM: Modal Excluir -->
									</td>
								</tr>
							<?php 
						}
					?>
					
					
				</tbody>
			</table>
		</div>
	</div>
</div>