<script type="text/javascript" src="<?php echo base_url(); ?>public/js/canvasjs.min.js"></script>
<div id="relatorios">
	<style type="text/css">
	.block button{
		width: 100%;
	}
	.bheader .block:first-child{
		margin-left: 20px;
	}
	a.canvasjs-chart-credit{
			visibility: hidden;
	}
	</style>
	<div class="box">
		<div class="bheader">
			<form action="<?php echo base_url(); ?>index.php/relatorios/pesquisar" method="GET" role="form">
				<div class="block">
					<span>Data Inicial</span>
					<div class="form-group">
						<label for="DiaInicial">Dia</label>
						<input type="date" class="form-control" name="DiaInicial" id="DiaInicial" value="<?php echo isset($DiaInicial) ? $DiaInicial : date('Y-m-d'); ?>">
					</div>
					<div class="form-group">
						<label for="HoraInicial">Hora</label>
						<input type="time" class="form-control" name="HoraInicial" id="HoraInicial" value="<?php echo isset($HoraInicial) ? $HoraInicial : '00:00'; ?>">
					</div>
				</div>
				<div class="block">
					<span>Data Final</span>
					<div class="form-group">
						<label for="DiaFinal">Dia</label>
						<input type="date" class="form-control" name="DiaFinal" id="DiaFinal" value="<?php echo isset($DiaFinal) ? $DiaFinal : date('Y-m-d'); ?>">
					</div>
					<div class="form-group">
						<label for="HoraFinal">Hora</label>
						<input type="time" class="form-control" name="HoraFinal" id="HoraFinal" value="<?php echo isset($HoraFinal) ? $HoraFinal : date('H:i',time()); ?>">
					</div>
				</div>
				<div class="block">
					<span>Pesquisar</span>
					<!--
					<div class="form-group">
						<label for="MediaPor">Media por</label>
						<select name="TipoMedia" class="selectBootstrap">
							<option value="hora">Hora</option>
							<option value="dia">Dia</option>
							<option value="mes">Mês</option>
						</select>
					</div>
					-->
					<div class="form-group">
						<button type="submit" class="btn btn-primary">Pesquisar</button>
					</div>
				</div>
			</form>
		</div>
		<?php 
			if(!isset($PesquisaON))
			{
				?>
					<div class="bcontent">
						<center><span>Efetue alguma pesquisa.</span></center>
					</div>
				<?php
			}
		?>
	</div>

	<?php 
		if(isset($PesquisaON))
		{
			foreach ($servidores as $servidor) 
			{
				
				?>
					<div class="box">
						<div class="bheader">
							<?php echo $servidor->nome; ?>
							<span class="ip">(<?php echo $servidor->ip; ?>)</span>
						</div>
						<div class="bcontent">
							<script type="text/javascript">
						        $.getJSON("<?php echo base_url('index.php/relatorios/getDados?IdServidor=').$servidor->id.'&dataInicio='.$DataInicio.'&dataFim='.$DataFim;?>", function (result) {
					        		//console.log("Data: "+result);
					                var chart<?php echo $servidor->id; ?> = new CanvasJS.Chart("chartContainer<?php echo $servidor->id; ?>", {
					                    
										toolTip:{             
											content: function(e){
												var content;
												var tempo = e.entries[0].dataPoint.x;
												tempo *= -1;
												content = "<label class='bold'>Latência (ms):</label> "+e.entries[0].dataPoint.y + "<hr class='canvas'> <label class='bold'>Tempo (Minutos Passados):</label> "+tempo;
												return content;
											},
										},
										data: [
					                        {
					                        	type: "<?php echo $TipoGrafico; ?>",
					                            dataPoints: result
					                        }
					                    ]
					                });

					                chart<?php echo $servidor->id; ?>.render();
					            });
						    </script>
							<div class="chartContainer" id="chartContainer<?php echo $servidor->id; ?>"></div>
						</div>
					</div>
				<?php 
			}/* End Foreach Servidores*/
		}
	?>
</div>