<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title><?php echo $title; ?></title>
	<meta charset="UTF-8">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/img/favicon.png" />
    <link href="<?php echo base_url(); ?>public/css/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>public/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>public/css/template.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>public/js/bootstrap.js"></script>
</head>
<body>
    <div class="header">
        <span><?php echo $this->session->userdata('nome'); ?></span>
    </div>
    <div class="dashboard">
    	<div class="presentation">
    		<img src="<?php echo base_url(); ?>public/img/logob.png">   
    	</div>
    	<ul class="sidemenu">
    		<li class="<?php echo $view_name == 'monitoramento' ? 'active' : ''; ?> " >
                <a href="<?php echo base_url(); ?>index.php/monitoramento">
                    <i class="fa fa-area-chart"></i>
                    <span>Monitoramento</span></a>
                <span class="arrow"></span>
            </li>
    		<li class="<?php echo $view_name == 'relatorios' ? 'active' : ''; ?> " >
                <a href="<?php echo base_url(); ?>index.php/relatorios">
                    <i class="fa fa-file-text-o"></i>
                    <span>Relatórios</span></a>
                <span class="arrow"></span>
            </li>
            <li class="<?php echo $view_name == 'usuarios' ? 'active' : ''; ?> " >
                <a href="<?php echo base_url(); ?>index.php/usuarios">
                    <i class="fa fa-users"></i>
                    <span>Usuários</span></a>
                <span class="arrow"></span>
            </li>
    		<li class="<?php echo $view_name == 'configuracoes' ? 'active' : ''; ?> " >
                <a href="<?php echo base_url(); ?>index.php/configuracoes">
                    <i class="fa fa-cogs"></i>
                    <span>Configurações</span></a>
                <span class="arrow"></span>
            </li>
    		<li class="<?php echo $view_name == 'sobre' ? 'active' : ''; ?> " >
                <a href="<?php echo base_url(); ?>index.php/sobre">
                    <i class="fa fa-info-circle"></i>
                    <span>Sobre</span></a>
                <span class="arrow"></span>
            </li>
    		<li class="<?php echo $view_name == 'sair' ? 'active' : ''; ?> " >
                <a href="<?php echo base_url(); ?>index.php/sair">
                    <i class="fa fa-times"></i>
                    <span>Sair</span></a>
                <span class="arrow"></span>
            </li>            
    	</ul>
    </div>

    <div class="content">
        <h2><?php echo $header_name; ?></h2>
    	<?php $this->load->view($view_name); ?>
    </div>

</body>
</html>
