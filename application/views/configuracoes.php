<div id="configuracoes">
	<style type="text/css">
		form .form-group label{
			padding-top: 6px;
			margin-right: 10px;
			float: left;
		}
		form .form-group input[type='text'],form .form-group select{
			width: 84%;
			margin-right: 10px;
			float: left;
		}
		.observacao{
			margin-bottom: 15px;
		}
	</style>
	<?php 
		foreach ($configuracoes as $config) {
			?>
				<div class="box">
					<div class="bheader">
						<?php echo $config->variavel; ?>
					</div>
					<div class="bcontent">
						<div class="observacao"><?php echo $config->observacao; ?></div>
						<form method="post" action="<?php echo base_url(); ?>index.php/configuracoes/editar" role="form">
							<input name="IdConfiguracao" type="hidden" value="<?php echo $config->id; ?>">
							<div class="form-group">
								<label for="ValorConfiguracao">Valor</label>
								<?php 
									$txt = "";
									if($config->modulo=="canvasjs")
									{
										$options = explode(";", $config->trigger);
										?>
										<select class="form-control" name="ValorConfiguracao">
											<?php 
												foreach ($options as $option) {
													$valores = explode("-", $option);
													if($config->valor==$valores[0])
													{
														echo "<option value='".$valores[0]."' selected='selected'>".$valores[1]."</option>";
													}
													else
													{
														echo "<option value='".$valores[0]."'>".$valores[1]."</option>";
													}
												}
											?>
										</select>
										<?php 
										echo $txt;
									}
									else{
										?> 
											<input type="text" class="form-control" name="ValorConfiguracao" id="ValorConfiguracao" value="<?php echo $config->valor; ?>">
										<?php
									}
								?>
								<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#configuracao-<?php echo $config->id; ?>">Alterar</button>

								<!-- Inicio Modal-->
								<div class="modal fade" id="configuracao-<?php echo $config->id; ?>" tabindex="-1" role="dialog" aria-labelledby="<?php echo $config->id; ?>Label" aria-hidden="true">
									<div class="modal-dialog">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
												<h4 class="modal-title" id="<?php echo $config->id; ?>Label">Alterar Variável: <?php echo $config->variavel; ?></h4>
											</div>
											<div class="modal-body">
												<div class="form-group">
													<p lingdex="0">Você tem certeza que deseja alterar essa configuração?</p>
												</div>

											</div>
											<div class="modal-footer">
												<button type="submit" class="btn btn-default"> Alterar</button>
												<button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
											</div>
										</div> <!-- FIM: div /.modal-content -->

									</div> <!-- FIM: div /.modal-dialog -->
								</div>
								<!-- Fim Inicio Modal-->	

							</div>

							
						</form>
					</div>
				</div>
			<?php 
		}
	?>
</div>