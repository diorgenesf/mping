<!DOCTYPE html>
<html lang="pt-br">
<head>
	<title>Acesso Restrito aos Usuários</title>
	<meta charset="UTF-8">
	<link rel="icon" type="image/png" href="<?php echo base_url(); ?>public/img/favicon.png" />
	<link href="<?php echo base_url(); ?>public/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>public/css/login.css" rel="stylesheet">
</head>
<body>
	<div class="container">

      <form class="form-signin" method="POST" action="<?php echo base_url(); ?>index.php/login/validation">
        <h2 class="form-signin-heading"><img src="<?php echo base_url(); ?>public/img/logo.png"></h2>
        <input name="username" type="text" class="input-block-level" placeholder="Username">
        <input name="password" type="password" class="input-block-level" placeholder="Password">
        <?php 
        	if($msg_error)
        	{
        		echo "<h6 class='form-error'>Usuário ou Senha inválidos!</h6>";
        	}
        ?>
        <button class="btn btn-large btn-primary" type="submit">Entrar</button>
      </form>

    </div>
</body>
</html>
