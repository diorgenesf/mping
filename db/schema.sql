-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 22/12/2014 às 16:15
-- Versão do servidor: 5.5.40-0ubuntu0.14.04.1
-- Versão do PHP: 5.5.9-1ubuntu4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de dados: `mping`
--

-- --------------------------------------------------------

--
-- Estrutura para tabela `configuracoes`
--

CREATE TABLE IF NOT EXISTS `configuracoes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `variavel` varchar(45) DEFAULT NULL,
  `valor` varchar(45) NOT NULL,
  `observacao` varchar(200) NOT NULL,
  `trigger` varchar(45) NOT NULL,
  `modulo` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Fazendo dump de dados para tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `variavel`, `valor`, `observacao`, `trigger`, `modulo`) VALUES
(1, 'Intervalo entre Pings (segundos)', '1', 'Aguarda N segundos de intervalo entre o envio de cada pacote.', '-i', 'ping'),
(2, 'Quantidade de Pings', '10', 'Define a quantidade de Pings que serão efetuados em cada servidor para se calcular a média.', '-c', 'ping'),
(3, 'Tamanho dos pacotes', '56', 'Especifica o número de bytes de dados a ser enviada. O padrão é 56, o que se traduz em 64 ICMP data bytes quando combinado com os 8 bytes de dados do cabeçalho ICMP.', '-s', 'ping'),
(4, 'Timeout', '10', 'Tempo de espera por uma resposta, em segundos. A opção afeta apenas o tempo de espera na ausência de qualquer resposta.', '-W', 'ping'),
(5, 'Tipo de Gráfico', 'spline', 'Esse campo permite alterar o tipo de gráfico que é exibido em monitoramento.', 'line-Linhas;spline-Curvas;stepLine-Quadrados', 'canvasjs');

-- --------------------------------------------------------

--
-- Estrutura para tabela `pings`
--

CREATE TABLE IF NOT EXISTS `pings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `servidor` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `media` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `servidor` (`servidor`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Estrutura para tabela `servidores`
--

CREATE TABLE IF NOT EXISTS `servidores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  `ip` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura para tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `nick` varchar(20) NOT NULL,
  `nome` varchar(150) NOT NULL,
  `senha` varchar(45) NOT NULL,
  PRIMARY KEY (`nick`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Fazendo dump de dados para tabela `usuarios`
--

INSERT INTO `usuarios` (`nick`, `nome`, `senha`) VALUES
('diorgenes.ferreira', 'Diorgenes Ferreira', '4badaee57fed5610012a296273158f5f');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
